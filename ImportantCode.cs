using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImportantCode : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        ImportantFunction();
        AnotherFunction();
    }

    void ImportantFunction()
    {
        Debug.Log("This is a important log");
    }

    void AnotherFunction()
    {
        Debug.Log("Another important execution");
    }
}
